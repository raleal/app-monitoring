package com.app.amazonas.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.app.amazonas.models.entity.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long>{

	public Usuario findByUsername(String username);
}
