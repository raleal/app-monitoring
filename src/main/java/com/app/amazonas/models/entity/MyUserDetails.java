package com.app.amazonas.models.entity;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.app.amazonas.models.entity.Usuario;

public class MyUserDetails extends org.springframework.security.core.userdetails.User{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Usuario usuario;

	public MyUserDetails(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, Usuario usuario) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.usuario = usuario;
	}
	
	public Usuario getUser() {
        return usuario;
    }

}
